package com.pharadon.lab6;

import static org.junit.Assert.assertEquals;



import org.junit.Test;

public class bookbanktest {
    @Test
    public void shouldwithdrawsuccess() {
        Bookbank book = new Bookbank("Pharadon", 100);
        book.withdraw(50);
        assertEquals(50, book.getBalance(),0.0001);
    }

    @Test
    public void shouldwithdrawovergetBalance() {
        Bookbank book = new Bookbank("Pharadon", 100);
        book.withdraw(150);
        assertEquals(100, book.getBalance(),0.0001);
    }

    @Test
    public void shouldwithdrawwithnehativenumber() {
        Bookbank book = new Bookbank("Pharadon", 100);
        book.withdraw(-100);
        assertEquals(100, book.getBalance(),0.0001);
    }
}
