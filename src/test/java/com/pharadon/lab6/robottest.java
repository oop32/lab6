package com.pharadon.lab6;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class robottest {
    @Test
    public void shouldDownOver() {
        Robot robot = new Robot("Robot", "R", 0, Robot.max_y);
        robot.down();
        assertEquals(false, robot.down());
        assertEquals(Robot.max_y, robot.getY());
    }

    @Test
    public void shouldDownnegative() {
        Robot robot = new Robot("Robot", "R", 0, Robot.min_y);
        assertEquals(false, robot.up());
        assertEquals(Robot.min_y, robot.getY());
    }

    @Test
    public void shouldDownSuccess() {
        Robot robot = new Robot("Robot", "R", 0, 0);
        assertEquals(true, robot.down());
        assertEquals(1, robot.getY());
    }

    @Test
    public void shouldUpSuccess() {
        Robot robot = new Robot("Robot", "R", 0, 1);
        assertEquals(true, robot.up());
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNSuccess2() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        boolean result = robot.up(11);
        assertEquals(true, result);
        assertEquals(0, robot.getY());
    }

    @Test
    public void shouldUpNFail1() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        boolean result = robot.up(12);
        assertEquals(false, result);
        assertEquals(0, robot.getY());
    }

    // down
    @Test
    public void shouldDownNSuccess1() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        boolean result = robot.down(6);
        assertEquals(true, result);
        assertEquals(17, robot.getY());
    }

    @Test
    public void shouldDownNFail1() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        boolean result = robot.down(12);
        assertEquals(false, result);
        assertEquals(19, robot.getY());
    }

    // left
    @Test
    public void shouldleftNSuccess1() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        boolean result = robot.left(7);
        assertEquals(true, result);
        assertEquals(3, robot.getX());
    }

    @Test
    public void shouldleftNFail1() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        boolean result = robot.left(20);
        assertEquals(false, result);
        assertEquals(0, robot.getX());
    }

    //right
    @Test
    public void shouldRightNSuccess1() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        boolean result = robot.right(7);
        assertEquals(true, result);
        assertEquals(17, robot.getX());
    }

    @Test
    public void shouldRightNFail1() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        boolean result = robot.right(20);
        assertEquals(false, result);
        assertEquals(19, robot.getX());
    }

    @Test
    public void shouldLeftSuccess() {
        Robot robot = new Robot("Robot", "R", 5, 5);
        assertEquals(true, robot.left());
        assertEquals(4, robot.getX());
    }

    @Test
    public void shouldRightSuccess() {
        Robot robot = new Robot("Robot", "R", 5, 5);
        assertEquals(true, robot.right());
        assertEquals(6, robot.getX());
    }

    @Test
    public void shouldCreateRobotSuccess1() {
        Robot robot = new Robot("Robot", "R", 10, 11);
        assertEquals("Robot", robot.getName());
        assertEquals("R", robot.getSymbol());
        assertEquals(10, robot.getX());
        assertEquals(11, robot.getY());
    }

    @Test
    public void shouldCreateRobotSuccess2() {
        Robot robot = new Robot("Robot", "R");
        assertEquals("Robot", robot.getName());
        assertEquals("R", robot.getSymbol());
        assertEquals(0, robot.getX());
        assertEquals(0, robot.getY());
    }

}
