package com.pharadon.lab6;

import javax.swing.plaf.basic.BasicTreeUI.TreeCancelEditingAction;

public class Robot {
    private String name;
    private String symbol;
    private int x;
    private int y;
    public static final int min_x = 0;
    public static final int min_y = 0;
    public static final int max_x = 19;
    public static final int max_y = 19;

    public Robot(String name, String string, int x, int y) {
        this.name = name;
        this.symbol = string;
        this.x = x;
        this.y = y;
    }

    public Robot(String name, String symbol) {
        this(name, symbol, 0, 0);
    }

    public boolean up() {
        if (y == min_y)
            return false;
        y = y - 1;
        return true;
    }

    public boolean left() {
        if (x == min_x)
            return false;
        x = x - 1;
        return true;
    }

    public boolean right() {
        if (x == max_x)
            return false;
        x = x + 1;
        return true;
    }


    // left
    public boolean left(int step) {
        for(int i = 0 ; i < step;i++){
            if(left() == false) {
                return false;
            }
        }
        return true;
    }

    public boolean up(int step) {
        for(int i = 0 ; i < step;i++){
            if(up() == false) {
                return false;
            }
        }
        return true;
    }

    public boolean down(int step) {
        for(int i = 0 ; i < step;i++){
            if(down() == false) {
                return false;
            }
        }
        return true;
    }


    public boolean right(int step) {
        for(int i = 0 ; i < step;i++){
            if(right() == false) {
                return false;
            }
        }
        return true;
    }

    public boolean down() {
        if (y == max_y)
            return false;
        y = y + 1;
        return true;
    }

    
    
    public void print() {
        System.out.println("Robot :" + name + " X " + x + " Y " + y);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
