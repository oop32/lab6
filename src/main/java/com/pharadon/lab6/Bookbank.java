package com.pharadon.lab6;

public class Bookbank {
    // attributes
    private String name;
    private double balance;

    public Bookbank(String name, double balance) { // Constructor เป็นตัวสร้าง ไม่มี return
        this.name = name;
        this.balance = balance;
    }

    // Method
    public boolean deposit(double money) {
        balance = balance + money;
        return true;
    }

    public boolean withdraw(double money) {
        if (money < 1)
            return false;
        if (money > balance)
            return false;
        balance = balance - money;
        return true;
    }

    public void print() {
        System.out.println(name + " " + balance);
    }

    public String getName(){
        return name;
    }

    public double getBalance(){
        return balance;
    }

    // ไม่มี static = object ; มี static = class
}
