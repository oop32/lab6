package com.pharadon.lab6;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bookbank pharadon = new Bookbank("Pharadon", 50.0);
        
        pharadon.print();

        Bookbank prayud = new Bookbank("Prayud",100000.0);
        prayud.print();
        prayud.withdraw(40000.0);
        prayud.print();

        pharadon.deposit(40000);
        System.out.println(pharadon.name + " " + pharadon.balance);

        Bookbank prawit = new Bookbank("Prawit",1000000.0);
        prawit.print();
        prawit.deposit(2);
        prawit.print();

    }
}
